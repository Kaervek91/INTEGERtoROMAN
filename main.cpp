// Romans convertor from integer numbers input 
/*
    COMMAND TO COMPILE WITH DEBUG g++ -ggdb -std=c++11 main.cpp -o main 
    MAIN RULES 
    Those roman numbers: I, X, C y M can be repeated to 3 times when compounding a roman number.
    Those roman numbers: V, L y D can't be repeated.
    If a roman number is compounded by a minor number on the left of a major number. Left one will substract its value from the major IX = 10 -1 = 9
    If a roman number is compounded by a major number on the left of a minor number. Both will be sumed XI = 10 + 1 = 11
*/
#include <iostream>
#include <string.h>

using namespace std;

string conversor (int number_int);



int main () {
    int decomp[] = {1000,900,500,400,100,90,50,40,10,9,8,7,6,5,4,3,2,1};
    int input,i;
    int previous = 0;
    int contador = 0;
    string roman = "";
    cout << "ENTER AN INT NUMBER: "; cin >> input; 
    for (i=0;i < sizeof(decomp);i++){
        if (input >= decomp[i]){
            input = input-decomp[i];
            roman +=conversor(decomp[i]);
            if(previous != 500 || previous != 50 || previous != 5){
                previous = decomp[i];
                if (previous == 1 || previous == 10 || previous == 100 || previous == 1000){
                    contador = contador + 1;
                    if(contador<3){
                        i=i-1;
                    } else {
                        contador = 0;
                    }
                }else {
                    i=i-1;
                }
            }
            
        }
    };
    cout << roman;
    cout << "\n";
    return 0;
}

string conversor(int number_int){
    string aux  = "";
    switch(number_int){
        case 1000:
            aux.append("M");
            break;
        case 900:
            aux.append("CM");
            break;
        case 500:
            aux.append("D");
            break;
        case 400:
            aux.append("CD");
            break;
        case 100:
            aux.append("C");
            break;
        case 90:
            aux.append("XC");
            break;
        case 50:
            aux.append("L");
            break;
        case 40:
            aux.append("XL");
            break;    
        case 10:
            aux.append("X");
            break;
        case 9:
            aux.append("IX");
            break;
        case 8:
            aux.append("VIII");
            break;
        case 7:
            aux.append("VII");
            break;
        case 6:
            aux.append("VI");
            break;
        case 5:
            aux.append("V");
            break;
        case 4:
            aux.append("IV");
            break;
        case 3:
            aux.append("III");
            break;
        case 2:
            aux.append("II");
            break;
        case 1:
            aux.append("I");
            break;
        default:
            return aux;
            break;
    }
    return aux;
}